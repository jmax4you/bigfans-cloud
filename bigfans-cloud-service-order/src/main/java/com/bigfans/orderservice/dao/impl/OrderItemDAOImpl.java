package com.bigfans.orderservice.dao.impl;

import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import com.bigfans.orderservice.dao.OrderItemDAO;
import com.bigfans.orderservice.model.OrderItem;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 
 * @Description:订单条目DAO
 * @author lichong
 * 2015年1月17日下午8:02:19
 *
 */
@Repository(OrderItemDAOImpl.BEAN_NAME)
public class OrderItemDAOImpl extends MybatisDAOImpl<OrderItem> implements OrderItemDAO {

	public static final String BEAN_NAME = "orderItemDAO";

	@Override
	public List<OrderItem> listByUserOrder(String userId, String orderId) {
		ParameterMap params = new ParameterMap();
		params.put("userId", userId);
		params.put("orderId", orderId);
		return getSqlSession().selectList(className + ".list", params);
	}

	@Override
	public List<OrderItem> listByOrder(String orderId) {
		ParameterMap params = new ParameterMap();
		params.put("orderId", orderId);
		return getSqlSession().selectList(className + ".list", params);
	}

	@Override
	public List<OrderItem> listUnCommentedByOrder(String userId, String orderId) {
		ParameterMap params = new ParameterMap();
		params.put("userId", userId);
		params.put("orderId", orderId);
		params.put("status", OrderItem.COMMENT_STATUS_UNCOMMENTED);
		return getSqlSession().selectList(className + ".list", params);
	}

	@Override
	public List<OrderItem> listByUser(String userId, Long start , Long pagesize) {
		ParameterMap params = new ParameterMap();
		params.put("userId", userId);
		params.put("pageable", true);
		params.put("start", start);
		params.put("pagesize", pagesize);
		return getSqlSession().selectList(className + ".list", params);
	}

	@Override
	public List<OrderItem> listByUser(String userId, Integer status, Long start , Long pagesize) {
		ParameterMap params = new ParameterMap();
		params.put("userId", userId);
		params.put("status", status);
		params.put("pageable", true);
		params.put("start", start);
		params.put("pagesize", pagesize);
		return getSqlSession().selectList(className + ".list", params);
	}
}
