package com.bigfans.cartservice.dao.impl;

import com.bigfans.cartservice.dao.CartItemDAO;
import com.bigfans.cartservice.model.CartItem;
import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository(CartItemDAOImpl.BEAN_NAME)
public class CartItemDAOImpl extends MybatisDAOImpl<CartItem> implements CartItemDAO {

	public static final String BEAN_NAME = "cartItemDAO";
	
	@Override
	public void increaseByUser(String uid , String pid , Integer quantity) {
		CartItem cartItem = new CartItem();
		cartItem.setUserId(uid);
		cartItem.setProdId(pid);
		cartItem.setQuantity(quantity);
		getSqlSession().update(className + ".increase", cartItem);
	}
	
	@Override
	public Long countByUser(String uid, String pid) {
		ParameterMap params = new ParameterMap();
		params.put("userId", uid);
		params.put("prodId", pid);
		return getSqlSession().selectOne(className + ".count", params);
	}

	@Override
	public Long sumByUser(String uid) {
		ParameterMap params = new ParameterMap();
		params.put("userId", uid);
		return getSqlSession().selectOne(className + ".sum", params);
	}
	
	@Override
	public CartItem loadByUser(String userId, String pid) {
		ParameterMap params = new ParameterMap();
		params.put("userId", userId);
		params.put("pid", pid);
		return getSqlSession().selectOne(className + ".loadByUser", params);
	}
	
	@Override
	public List<CartItem> getItems(String userId , boolean selected) {
		ParameterMap params = new ParameterMap();
		params.put("userId", userId);
		params.put("isSelected", selected);
		return getSqlSession().selectList(className + ".listByUser", params);
	}
	
	@Override
	public List<CartItem> listByUser(String userId, Long start, Long pagesize) {
		ParameterMap params = new ParameterMap(start, pagesize);
		params.put("userId", userId);
		return getSqlSession().selectList(className + ".listByUser", params);
	}

	@Override
	public void changeItemSelectStatus(String userId, String productId, Boolean selected) {
		ParameterMap params = new ParameterMap();
		params.put("userId", userId);
		params.put("prodId", productId);
		params.put("selected", selected);
		getSqlSession().update(className + ".changeSelectStatus", params);
	}

	@Override
	public void changeAllItemsSelectStatus(String userId, Boolean selected) {
		ParameterMap params = new ParameterMap();
		params.put("userId", userId);
		params.put("selected", selected);
		getSqlSession().update(className + ".changeSelectStatus", params);
	}
	
	@Override
	public void deleteMyItemsById(String userId , String[] ids) {
		ParameterMap params = new ParameterMap();
		params.put("userId", userId);
		params.put("ids", ids);
		getSqlSession().delete(className + ".deleteMyItems", params);
	}
	
	@Override
	public void deleteSelectedItems(String userId) {
		ParameterMap params = new ParameterMap();
		params.put("userId", userId);
		params.put("selected", true);
		getSqlSession().delete(className + ".deleteMyItems", params);
	}
	
}
