package com.bigfans.model.event.order;

import com.bigfans.framework.event.AbstractEvent;
import lombok.Data;

@Data
public class OrderPayTimeExpiredEvent extends AbstractEvent {

    private String orderId;

    public OrderPayTimeExpiredEvent(String orderId) {
        this.orderId = orderId;
    }
}
