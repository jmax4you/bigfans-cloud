package com.bigfans.searchservice.api;

import com.bigfans.framework.model.FTSPageBean;
import com.bigfans.framework.model.PageContext;
import com.bigfans.framework.web.BaseController;
import com.bigfans.framework.web.RestResponse;
import com.bigfans.searchservice.model.Brand;
import com.bigfans.searchservice.model.Category;
import com.bigfans.searchservice.service.BrandService;
import com.bigfans.searchservice.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 品牌搜索服务
 *
 * @author lichong
 */
@RestController
public class CategorySearchApi extends BaseController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/categories")
    public RestResponse search(
            @RequestParam(value = "q") String keyword,
            @RequestParam(value = "cp", required = false, defaultValue = "1") Integer cp) throws Exception {
        PageContext.setCurrentPage(cp.longValue());
        PageContext.setPageSize(10L);
        FTSPageBean<Category> ftsPageBean = categoryService.search(keyword, PageContext.getStart().intValue(), PageContext.getPageSize().intValue());
        List<Category> categories = ftsPageBean.getData();
        return RestResponse.ok(categories);
    }
}
